insert into users
  values(DEFAULT, 'Bengt', 1442014879, 'bengt@example.com', 0);
insert into users
  values(DEFAULT, 'Ole', 76275215, 'ole@example.com', 0);
insert into users
  values(DEFAULT, 'Klaas', 1027831993, 'klaas@example.com', 0);


--insert into photos
--  values('http://i.imgur.com/LaOpp.jpg', 256, 'http://imgur.com/delete/uUarHZeSdxy6PCc');
--insert into photos
--  values('http://i.imgur.com/v7dBJ.jpg', 256, 'http://imgur.com/delete/Y4dktDVthaGzTGQ');
--insert into photos
--  values('http://i.imgur.com/qtS7v.jpg', 256, 'http://imgur.com/delete/dsoRUqaQrCqdvrn');
--insert into photos
--  values('http://i.imgur.com/NU97Z.jpg', 256, 'http://imgur.com/delete/8DpQ845pK2KLjYs');
--insert into photos
--  values('http://i.imgur.com/5ctKt.jpg', 256, 'http://imgur.com/delete/TYucgIP1GKQBd4h');


insert into animals
  values(DEFAULT, 'EarlyOwl', 'http://i.imgur.com/LaOpp.jpg', 'Oldenburg');
insert into animals
  values(DEFAULT, 'hoverPhant', 'http://i.imgur.com/v7dBJ.jpg', 'Nottingham');
insert into animals
  values(DEFAULT, 'lowl', 'http://i.imgur.com/qtS7v.jpg', 'Bremen');
insert into animals
  values(DEFAULT, 'stareCat', 'http://i.imgur.com/NU97Z.jpg', 'Washington D.C.');
insert into animals
  values(DEFAULT, 'sunDog', 'http://i.imgur.com/5ctKt.jpg', 'Canberra');


insert into locations
  values(DEFAULT, 1, timestamp('2010-01-01 10:05:45'), 8.199777773997619, 53.137349321146694);
insert into locations
  values(DEFAULT, 2, timestamp('2010-02-01 11:05:17'), -1.133333, 52.95);
insert into locations
  values(DEFAULT, 3, timestamp('2010-02-01 11:06:17'), 8.8, 53.0);
insert into locations
  values(DEFAULT, 4, timestamp('2010-02-01 11:06:17'), -77.0, 38.9);
insert into locations
  values(DEFAULT, 5, timestamp('2010-02-01 11:06:17'), 149.1, -35.3);


insert into events
  values(DEFAULT, 0, timestamp('2010-10-04 11:06:17'), null, null, 1, 2, 'Hello hoverPhant! How are you on this fine day?');
insert into events
  values(DEFAULT, 0, timestamp('2010-10-04 16:57:39'), null, null, 3, 2, 'OMGWTFBBQ, your nose is soo HUGE!');
insert into events
  values(DEFAULT, 0, timestamp('2010-10-04 16:57:40'), null, null, 1, 2, 'text 1');
insert into events
  values(DEFAULT, 0, timestamp('2010-10-04 16:57:41'), null, null, 2, 2, 'text 2');
insert into events
  values(DEFAULT, 0, timestamp('2010-10-04 16:57:42'), null, null, 3, 2, 'text 3');
insert into events
  values(DEFAULT, 0, timestamp('2010-10-04 16:57:43'), null, null, 4, 2, 'text 4');
insert into events
  values(DEFAULT, 0, timestamp('2010-10-04 16:57:44'), null, null, 5, 2, 'text 5');
insert into events
  values(DEFAULT, 1, timestamp('2009-01-04 12:30:01'), null, null, 1, 2, null);
insert into events
  values(DEFAULT, 1, timestamp('2009-01-04 12:30:01'), null, null, 2, 1, null);
insert into events
  values(DEFAULT, 1, timestamp('2009-03-03 17:22:25'), null, null, 2, 3, null);
insert into events
  values(DEFAULT, 1, timestamp('2009-03-03 17:22:25'), null, null, 3, 2, null);
insert into events
  values(DEFAULT, 1, timestamp('2010-03-03 17:22:25'), null, null, 3, 4, null);
insert into events
  values(DEFAULT, 1, timestamp('2010-03-03 17:22:25'), null, null, 4, 3, null);
insert into events
  values(DEFAULT, 1, timestamp('2010-05-06 17:22:25'), null, null, 4, 5, null);
insert into events
  values(DEFAULT, 1, timestamp('2010-05-06 17:22:25'), null, null, 5, 4, null);


insert into animal_friend
  values(1, 2, 1);
insert into animal_friend
  values(2, 3, 1);
insert into animal_friend
  values(3, 4, 1);
insert into animal_friend
  values(4, 5, 1);

insert into user_animal
  values(1, 1);
insert into user_animal
  values(1, 5);
insert into user_animal
  values(2, 2);
insert into user_animal
  values(3, 3);
